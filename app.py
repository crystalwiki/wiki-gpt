import os
import subprocess
from datetime import datetime

import constants
import wikigpt
from flask import Flask, make_response, render_template, redirect, request, url_for
from flask_sqlalchemy import SQLAlchemy
from utils import moderate_response

app = Flask(__name__)
project_dir = os.path.dirname(os.path.abspath(__file__))
database_file = "sqlite:///{}".format(
    os.path.join(project_dir, constants.DATABASE_NAME)
)
app.config["SQLALCHEMY_DATABASE_URI"] = database_file
db = SQLAlchemy(app)


class Searches(db.Model):
    # search_id | search_query | wipedia_url | wikipedia_corpus | search_results | search_url | search_timestamp
    search_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    search_query = db.Column(db.String(1000))
    wipedia_url = db.Column(db.String(5000))
    wikipedia_corpus = db.Column(db.String(5000))
    search_results = db.Column(db.String(1000))
    search_url = db.Column(db.String(100), unique=True)
    search_timestamp = db.Column(db.DateTime, default=datetime.utcnow())


def insert_db_search(
    search_query, wipedia_url, wikipedia_corpus, search_results, search_url
):
    wipedia_url_str = "\n".join(wipedia_url)  # Convert list to string to store in db
    search = Searches(
        search_query=search_query,
        wipedia_url=wipedia_url_str,
        wikipedia_corpus=wikipedia_corpus,
        search_results=search_results,
        search_url=search_url,
    )
    db.session.add(search)
    db.session.commit()


def query_db_search(search_url):
    db_search_results = (
        db.session.query(Searches)
        .filter_by(search_url=search_url)
        .with_entities(
            Searches.search_id,
            Searches.search_query,
            Searches.wipedia_url,
            Searches.wikipedia_corpus,
            Searches.search_results,
            Searches.search_url,
            Searches.search_timestamp,
        )
        .first()
    )
    return db_search_results


def queryWikiGPT(search_query):
    # Get the wikipedia article urls
    wikipedia_article_urls = wikigpt.get_wikipedia_article_urls(
        search_query,
        constants.GOOGLE_SEARCH_API_KEY,
        constants.GOOGLE_SEARCH_ENGINE_ID,
    )
    # Get the wikipedia text
    num_of_articles_considered = 3
    article_urls_to_consider = wikipedia_article_urls[:num_of_articles_considered]
    wikipedia_text = "\n\n".join(
        [wikigpt.get_wikipedia_text(url) for url in article_urls_to_consider]
    )
    # Create the prompt
    prompt = wikigpt.create_prompt(
        search_query, wikipedia_text, article_urls_to_consider
    )
    # Create the response
    response = wikigpt.create_response(prompt)
    # Search results text
    search_results = response["choices"][0]["text"]
    return search_results, article_urls_to_consider, wikipedia_text


@app.route("/", methods=["POST", "GET"])
def login():
    if constants.AUTHORIZATION_COOKIE_KEY in request.cookies:
        response = make_response(render_template("login.html"))
        response.set_cookie(constants.AUTHORIZATION_COOKIE_KEY, "", expires=0)
        return response
    else:
        if request.method == "POST":
            login_pwd = request.form["login"]  # TODO: add sanitization
            if login_pwd == constants.LOGIN_PASSWORD:
                response = make_response(redirect(url_for("search")))
                response.set_cookie(
                    constants.AUTHORIZATION_COOKIE_KEY,
                    constants.AUTHORIZATION_COOKIE_VALUE,
                )
                return response
            else:
                wrong_pwd = "Oops! Please enter the correct password!"
                template = render_template("login.html", wrong_pwd=wrong_pwd)
        else:
            template = render_template("login.html")
        return template


@app.route("/search", methods=["POST", "GET"])
def search():
    access_restriction_cookie = request.cookies.get(constants.AUTHORIZATION_COOKIE_KEY)
    if access_restriction_cookie == constants.AUTHORIZATION_COOKIE_VALUE:
        if request.method == "POST":
            search_query = request.form["search"]  # TODO: add sanitization
            search_results, wikipedia_urls, wikipedia_corpus = queryWikiGPT(
                search_query
            )
            search_url = constants.encoded_url()
            insert_db_search(
                search_query,
                wikipedia_urls,
                wikipedia_corpus,
                search_results,
                search_url,
            )

            search_results = moderate_response(search_results)
            template = render_template(
                "search.html",
                search_query=search_query,
                search_results=search_results,
                wikipedia_urls=wikipedia_urls,
                wikipedia_corpus=wikipedia_corpus,
                search_url=search_url,
            )
        else:
            template = render_template("search.html")
        return template
    else:
        return redirect(url_for("login"))


@app.route("/search/<search_url>", methods=["POST", "GET"])
def searchURL(search_url):
    access_restriction_cookie = request.cookies.get(constants.AUTHORIZATION_COOKIE_KEY)
    if access_restriction_cookie == constants.AUTHORIZATION_COOKIE_VALUE:
        (
            search_id,
            search_query,
            wipedia_url,
            wikipedia_corpus,
            search_results,
            search_url,
            search_timestamp,
        ) = query_db_search(search_url)
        wipedia_url = wipedia_url.split(
            "\n"
        )  # convert string to list for template rendering
        template = render_template(
            "search.html",
            search_query=search_query,
            wikipedia_urls=wipedia_url,
            search_results=search_results,
            wikipedia_corpus=wikipedia_corpus,
            search_url=search_url,
        )
        return template
    else:
        return redirect(url_for("login"))


@app.route("/update-server", methods=["POST"])
def webhook():
    if request.method == "POST":
        subprocess.check_output(["git", "pull", "origin", "main"])
        return "Updated Toolforge project successfully", 200
    else:
        return "Wrong event type", 400


if __name__ == "__main__":
    app.app_context().push()
    db.create_all()
    app.run(debug=True)
