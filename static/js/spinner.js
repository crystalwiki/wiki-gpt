function activateSpinner() {
  let searchBtn = document.getElementById("searchBtn");
  searchBtn.disabled = true;
  let wpLogo = document.getElementById("wpLogo");
  wpLogo.src = "../static/images/wikipedia-logo-spinning-globe.gif";
}
