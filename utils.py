import requests
from constants import OPENAI_KEY


def create_hyperlink(url: str, title: str = None):
    link = f'<a href="{url}">{title if title else url}</a>'
    return link


def moderate_response(text: str) -> str:
    url = "https://api.openai.com/v1/moderations"
    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {OPENAI_KEY}",
    }
    response = requests.post(
        url, headers=headers, data=f'{{"input": "{text.encode("utf-8")}"}}'
    )

    if response.status_code == 200 and response.json()["results"][0]["flagged"]:
        return "There seems to be a problem with the response as it is flagged as inappropriate."
    else:
        return text
